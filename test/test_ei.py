# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

from typing import Type, Union
from pathlib import Path

import snegg.ei
import snegg.eis

import os
import select
import pytest
import socket
import time


@pytest.fixture
def socketpath(tmp_path: Path) -> Path:
    return tmp_path / "eis-0"


@pytest.fixture
def ei_sender(socketpath: Path) -> snegg.ei.Context:
    return snegg.ei.Sender.create_for_socket(path=socketpath)


@pytest.fixture
def ei_receiver(socketpath: Path) -> snegg.ei.Context:
    return snegg.ei.Receiver.create_for_socket(path=socketpath)


@pytest.fixture
def eis_context(socketpath: Path) -> snegg.eis.Eis:
    return snegg.eis.Eis.create_for_socket(socketpath)


@pytest.fixture
def eis_ei_sender(
    eis_context: snegg.eis.Eis, ei_sender: snegg.ei.Context
) -> tuple[snegg.eis.Eis, snegg.ei.Context]:
    """
    Returns a pre-dispatched eis + ei pair, i.e. the handshake is complete and the
    snegg.eis.EventType.CLIENT_CONNECT event is waiting.
    """
    dispatch_both(eis_context, ei_sender)
    return eis_context, ei_sender


@pytest.fixture
def eis_ei_receiver(
    eis_context: snegg.eis.Eis, ei_receiver: snegg.ei.Context
) -> tuple[snegg.eis.Eis, snegg.ei.Context]:
    """
    Returns a pre-dispatched eis + ei pair, i.e. the handshake is complete and the
    snegg.eis.EventType.CLIENT_CONNECT event is waiting.
    """
    dispatch_both(eis_context, ei_receiver)
    return eis_context, ei_receiver


def dispatch_both(eis: snegg.eis.Eis, ei: snegg.ei.Context) -> None:
    """
    We sometimes require roundtrips (e.g. during connection setup),
    so a triple dispatch on both eis + ei should be enough to have the
    required events waiting.
    """
    for _ in range(3):
        eis.dispatch()
        ei.dispatch()


class TestEi:
    @pytest.mark.parametrize("context_type", ("sender", "receiver"))
    @pytest.mark.parametrize("use_env", (True, False))
    def test_context_load(self, monkeypatch, tmp_path, context_type, use_env):
        socketpath = tmp_path / "eis-0"
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.bind(os.fspath(socketpath))
        sock.listen()

        # make sure the socket shows up in the fs
        while not socketpath.exists():
            time.sleep(0.1)

        cls: Union[Type[snegg.ei.Sender], Type[snegg.ei.Receiver]]

        if context_type == "sender":
            cls = snegg.ei.Sender
        else:
            cls = snegg.ei.Receiver

        if use_env:
            monkeypatch.setenv("LIBEI_SOCKET", os.fspath(socketpath))
            socketpath = None

        context = cls.create_for_socket(path=socketpath, name=context_type)
        assert context.name == context_type

        # basic properties all contexts have
        now = int(time.clock_gettime(time.CLOCK_MONOTONIC) * 1e6)
        assert context.now >= now
        assert context.fd is not None

        # We have a valid socket, so there shouldn't be any events
        events = list(context.events)
        assert not events, f"Unexpected events in event queue: {events}"

        # Definitely don't expect anything to have happend on the fd
        poll = select.poll()
        poll.register(context.fd)
        context.dispatch()
        assert poll.poll(200) == []

        # Close the server socket
        sock.close()
        context.dispatch()
        events = list(context.events)
        assert len(events) == 1, "Expected to get disconnected"
        event = events.pop(0)
        assert event.event_type == snegg.ei.EventType.DISCONNECT

    def test_add_remove_seat(self, eis_ei_sender):
        eis, ei = eis_ei_sender

        # expect the client connect event and nothing else
        events = list(eis.events)
        assert len(events) == 1
        for event in events:
            assert event.event_type == snegg.eis.EventType.CLIENT_CONNECT
            client = event.client
            client.connect()
            seat = client.new_seat()
            seat.add()
            seat.remove()

        dispatch_both(eis, ei)

        events = list(ei.events)
        assert len(events) == 3  # connect, seat added and removed
        event = events.pop(0)
        assert event.event_type == snegg.ei.EventType.CONNECT

        def check_seat(seat: snegg.ei.Seat):
            assert seat is not None
            assert seat.capabilities == tuple(snegg.ei.DeviceCapability)
            assert seat.name == "default seat"

        event = events.pop(0)
        assert event.event_type == snegg.ei.EventType.SEAT_ADDED
        added_seat = event.seat
        check_seat(added_seat)

        event = events.pop(0)
        assert event.event_type == snegg.ei.EventType.SEAT_REMOVED
        removed_seat = event.seat
        assert removed_seat == added_seat
        check_seat(removed_seat)

    def test_eis_region(self, eis_ei_sender):
        eis, ei = eis_ei_sender

        events = list(eis.events)
        event = events.pop(0)
        assert event.event_type == snegg.eis.EventType.CLIENT_CONNECT
        eis_client = event.client
        eis_client.connect()
        eis_seat = eis_client.new_seat()
        eis_seat.add()

        dispatch_both(eis, ei)

        events = list(ei.events)
        event = events.pop(0)
        assert event.event_type == snegg.ei.EventType.CONNECT
        event = events.pop(0)
        assert event.event_type == snegg.ei.EventType.SEAT_ADDED
        ei_seat = event.seat
        ei_seat.bind()

        dispatch_both(eis, ei)

        events = list(eis.events)
        event = events.pop(0)
        assert event.event_type == snegg.eis.EventType.SEAT_BIND
        assert event.seat == eis_seat

        # Create a device with regions and verify they exist on the actual device
        configure_regions = [
            snegg.eis.ConfigureRegion(dimension=snegg.eis.Dimension(640, 480)),
            snegg.eis.ConfigureRegion(
                dimension=snegg.eis.Dimension(1920, 1080),
                position=snegg.eis.Position(640, 480),
                # This will be a noop on libei 1.0
                mapping_id="mid1234",
            ),
        ]
        eis_device = eis_seat.new_device(regions=configure_regions)

        def check_region(r: Union[snegg.eis.Region, snegg.ei.Region]):
            # Check the edges for region.contains
            assert r.contains(r.position.x, r.position.y)
            assert not r.contains(r.position.x - 0.01, r.position.y)
            assert not r.contains(r.position.x, r.position.y - 0.01)
            assert not r.contains(
                r.position.x + r.dimension.width, r.position.y + r.dimension.height
            )
            assert r.contains(
                r.position.x + r.dimension.width - 0.01,
                r.position.y + r.dimension.height - 0.01,
            )
            assert not r.contains(
                r.position.x + r.dimension.width,
                r.position.y + r.dimension.height - 0.01,
            )
            assert not r.contains(
                r.position.x + r.dimension.width - 0.01,
                r.position.y + r.dimension.height,
            )

            # On libei 1.0, the mapping id is always None, so all we can
            # check for is that if it is set, it is set to the correct value.
            if r.mapping_id is not None:
                assert r.mapping_id == "mid1234"

        for r in eis_device.regions:
            assert (r.position, r.dimension) in [
                (cr.position, cr.dimension) for cr in configure_regions
            ]
            check_region(r)

        eis_device.add()

        dispatch_both(eis, ei)

        # Verify they exist on the client side too
        events = list(ei.events)
        event = events.pop(0)
        assert event.event_type == snegg.ei.EventType.DEVICE_ADDED
        ei_device = event.device

        for r in ei_device.regions:
            # Can't compare snegg.ei.Position with snegg.eis.Position and it's better if we can't anyway
            pos = snegg.eis.Position(r.position.x, r.position.y)
            dim = snegg.eis.Dimension(r.dimension.width, r.dimension.height)
            assert (pos, dim) in [
                (cr.position, cr.dimension) for cr in configure_regions
            ]

            check_region(r)

            # Check some points region.convert_point
            assert r.convert(r.position.x, r.position.y) == (0, 0)
            assert r.convert(r.position.x + 10, r.position.y + 30) == (10, 30)
            assert r.convert(r.position.x - 1, r.position.y) == ()
