
snegg Python wrapper for libei
==============================

**snegg** is a wrapper around the libei C library, with a
pythonic API.

.. toctree::
   :caption: Table of Contents
   :maxdepth: 2

   API documentation <modules>

Source code
-----------

The source code for this project is available at
https://gitlab.freedesktop.org/whot/snegg

Installation
------------

A requirement for **snegg** to work is that the ``libei``, ``libeis`` or
``liboeffis`` C library is installed on your system (whichever is to be used).
Install through your favourite package managers, e.g. ``dnf install libei``.

**snegg** can be installed via ``pip3``, directly from
the git repository::

    # Install from the repo directly
    $ pip3 install git+http://gitlab.freedesktop.org/whot/snegg

    # Install from a local checkout
    $ git clone https://gitlab.freedesktop.org/whot/snegg
    $ cd snegg
    $ pip3 install .

For more details on using pip and the PyPI, please see https://pypi.python.org/pypi

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
