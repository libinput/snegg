doc:
	sphinx-apidoc --separate --force --module-first -d 1 -o doc/ snegg snegg/c/*
	sphinx-build -a -b html doc/ build/

test:
	PYTHONPATH=. pytest-3 test/*.py

.PHONY: doc test
