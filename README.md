# snegg

`snegg` provides Python bindings for libei, the library for Emulated Input, see
https://libinput.pages.freedesktop.org/libei.
It requires that libei, liboeffis or libeis are installed at runtime (whichever
one is used by the application).

API Documentation is available [here](https://libinput.pages.freedesktop.org/snegg/)

**The snegg API is not stable**

This is not (yet?) a pure Python implementation, it merely wraps the C library
via ctypes and it does so in the (almost) simplest way possible. Some
accommodations are made to make the API more Pythonic, in particular the
various calls to `ei_foo_get_bar` are usually the `Foo.bar` property in the
Python API.

See the `examples/` directory for examples on how to use this API.

Currently the goal of snegg is to allow for rapid prototyping of EI clients or
EIS implementations to explore the possible application space.

## A note on lifetimes

The Python objects in snegg wrap the respective C object. libei(s) require that
each object calls `ei_foo_unref()` to dispose of the object - snegg handles
this automatically through Python weakrefs and attempts to return the same
Python object every time.

However, unless the Python object is kept alive, the next time a C object must
be wrapped it may return a different Python object. See the
`examples/ei-reference-change.py` example to illustrate this issue.
