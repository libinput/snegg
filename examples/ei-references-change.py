#!/usr/bin/env python3

import os
import select
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import snegg.ei as ei


def main():
    ctx = ei.Sender.create_for_socket(path=None, name="ei-example")
    poll = select.poll()
    poll.register(ctx.fd)
    while poll.poll():
        ctx.dispatch()
        for e in ctx.events:
            # The seat object is automatically unref'd when the
            # event goes out of scope. On the next event, the
            # event.seat Python object may not be the same even
            # though the underlying C object is identical.
            # This applies to most objects returned by libei.
            #
            # This can be avoided by keeping the Python object
            # alive, e.g. some local `seat = e.seat` assignment
            # that does not go out of scope.
            if e.event_type == ei.EventType.SEAT_ADDED:
                assert e.seat is not None
                print(f"{id(e.seat)} - {e.seat}")
                e.seat.bind(e.seat.capabilities)
            if e.event_type == ei.EventType.DEVICE_ADDED:
                assert e.seat is not None
                print(f"{id(e.seat)} - {e.seat}")


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
