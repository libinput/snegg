#!/usr/bin/env python3
# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

from typing import Optional
from pathlib import Path
import argparse
import logging
import os
import select
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from snegg.eis import Eis, EventType, DeviceCapability

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def main(socket: Optional[Path]):
    if not socket:
        xdg = os.getenv("XDG_RUNTIME_DIR")
        assert xdg, "XDG_RUNTIME_DIR must be set"
        socket = Path(xdg) / "eis-0"
    else:
        assert socket is not None
    eis = Eis.create_for_socket(socket)

    poll = select.poll()
    poll.register(eis.fd)
    while poll.poll():
        eis.dispatch()
        for e in eis.events:
            print(e)
            if e.event_type == EventType.CLIENT_CONNECT:
                client = e.client
                client.connect()
                # Create a seat with all capabalities
                seat = client.new_seat()
                seat.add()
            elif e.event_type == EventType.SEAT_BIND:
                seat = e.seat
                assert seat is not None
                caps = e.seat_event.capabilities
                if DeviceCapability.POINTER in caps:
                    device = seat.new_device(
                        name="pointer device",
                        capabilities=[
                            DeviceCapability.POINTER,
                            DeviceCapability.BUTTON,
                            DeviceCapability.SCROLL,
                        ],
                    )
                    device.add()
                    device.resume()
                if DeviceCapability.KEYBOARD in caps:
                    device = seat.new_device(
                        name="keyboard device", capabilities=[DeviceCapability.KEYBOARD]
                    )
                    device.add()
                    device.resume()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--socket", type=Path, default=None)
    ns = parser.parse_args()

    try:
        main(ns.socket)
    except KeyboardInterrupt:
        pass
