# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

"""
Wrapper module around the ``libei`` C library. This is a thin API wrapper with
most of the semantics of the underlying C library preserved.
See the `libei documentation <https://libinput.pages.freedesktop.org/libei/api/group__libei.html>`_
for details on each API.

.. warning:: Most objects in this module are refcounted and automatically destroy
             the underlying C object when the Python reference is dropped.
             This may cause all sub-objects in the underlying C object to be
             destroyed as well. Care must be taken to preserve the
             Python object across multiple invocations.
"""

from typing import Iterator, IO, Optional, Union
from ctypes import c_void_p, c_int, c_char_p, CFUNCTYPE
from ._cobject import unref, ref_unref, CObjectWrapper
from pathlib import Path
from functools import reduce
import dataclasses
import enum
import logging
import os
import time

logger = logging.getLogger("ei")

from snegg.c.libei import libei


class ConnectionError(Exception):
    def __init__(self, message: str, errno: Optional[int] = None):
        self.errno = errno
        self.message = message


class _LogPriority(enum.IntEnum):
    DEBUG = 10
    INFO = 20
    WARNING = 30
    ERROR = 40


class EventType(enum.IntEnum):
    CONNECT = 1
    DISCONNECT = 2
    SEAT_ADDED = 3
    SEAT_REMOVED = 4
    DEVICE_ADDED = 5
    DEVICE_REMOVED = 6
    DEVICE_PAUSED = 7
    DEVICE_RESUMED = 8
    KEYBOARD_MODIFIERS = 9
    FRAME = 100
    DEVICE_START_EMULATING = 200
    DEVICE_STOP_EMULATING = 201
    POINTER_MOTION = 300
    POINTER_MOTION_ABSOLUTE = 400
    BUTTON_BUTTON = 500
    SCROLL_DELTA = 600
    SCROLL_STOP = 601
    SCROLL_CANCEL = 602
    SCROLL_DISCRETE = 603
    KEYBOARD_KEY = 700
    TOUCH_DOWN = 800
    TOUCH_UP = 801
    TOUCH_MOTION = 802


class DeviceCapability(enum.IntFlag):
    POINTER = 1 << 0
    POINTER_ABSOLUTE = 1 << 1
    KEYBOARD = 1 << 2
    TOUCH = 1 << 3
    SCROLL = 1 << 4
    BUTTON = 1 << 5

    @staticmethod
    def all() -> int:
        """
        Return the bitmask of all known capabilities
        """
        return reduce(lambda mask, x: x | mask, DeviceCapability)


class DeviceType(enum.IntEnum):
    VIRTUAL = 1
    PHYSICAL = 2


class KeymapType(enum.IntEnum):
    XKB = 1


@dataclasses.dataclass
class XkbModifiersEvent:
    depressed: int
    latched: int
    locked: int
    group: int


@dataclasses.dataclass
class KeyEvent:
    key: int
    is_press: bool


@dataclasses.dataclass
class ButtonEvent:
    button: int
    is_press: bool


@dataclasses.dataclass
class PointerEvent:
    dx: float
    dy: float


@dataclasses.dataclass
class PointerAbsoluteEvent:
    x: float
    y: float


@dataclasses.dataclass
class ScrollEvent:
    dx: float
    dy: float


@dataclasses.dataclass
class ScrollDiscreteEvent:
    dx: int
    dy: int


@dataclasses.dataclass
class ScrollStopEvent:
    x: bool
    y: bool


@dataclasses.dataclass
class TouchEvent:
    touchid: int
    x: float
    y: float


@unref(libei)
class Event(CObjectWrapper):
    def __init__(self, cobject):
        super().__init__(cobject)
        self._event_type = EventType(libei.event_get_type(cobject))
        self._time = libei.event_get_time(self._cobject)
        d = libei.event_get_device(self._cobject)
        self._device = Device.instance(d) if d else None
        s = libei.event_get_seat(self._cobject)
        self._seat = Seat.instance(s) if s else None

    def __str__(self) -> str:
        return f"Event: {self.event_type.name}"

    def __repr__(self) -> str:
        return str(self)

    @property
    def event_type(self) -> EventType:
        return self._event_type

    @property
    def time(self) -> int:
        return self._time

    @property
    def device(self) -> Optional["Device"]:
        return self._device

    @property
    def seat(self) -> Optional["Seat"]:
        return self._seat

    @property
    def emulating_sequence(self) -> int:
        return libei.event_emulating_get_sequence(self._cobject)

    @property
    def keyboard_xkb_modifiers(self) -> XkbModifiersEvent:
        return XkbModifiersEvent(
            depressed=libei.event_keyboard_get_xkb_mods_depressed(self._cobject),
            latched=libei.event_keyboard_get_xkb_mods_latched(self._cobject),
            locked=libei.event_keyboard_get_xkb_mods_locked(self._cobject),
            group=libei.event_keyboard_get_xkb_group(self._cobject),
        )

    @property
    def key_event(self) -> KeyEvent:
        return KeyEvent(
            key=libei.event_keyboard_get_key(self._cobject),
            is_press=libei.event_keyboard_get_key_is_press(self._cobject),
        )

    @property
    def button_event(self) -> ButtonEvent:
        return ButtonEvent(
            button=libei.event_button_get_button(self._cobject),
            is_press=libei.event_button_get_is_press(self._cobject),
        )

    @property
    def pointer_event(self) -> PointerEvent:
        return PointerEvent(
            dx=libei.event_pointer_get_dx(self._cobject),
            dy=libei.event_pointer_get_dy(self._cobject),
        )

    @property
    def pointer_absolute_event(self) -> PointerAbsoluteEvent:
        return PointerAbsoluteEvent(
            x=libei.event_pointer_get_absolute_x(self._cobject),
            y=libei.event_pointer_get_absolute_y(self._cobject),
        )

    @property
    def scroll_event(self) -> ScrollEvent:
        return ScrollEvent(
            dx=libei.event_scroll_get_dx(self._cobject),
            dy=libei.event_scroll_get_dy(self._cobject),
        )

    @property
    def scroll_discrete_event(self) -> ScrollDiscreteEvent:
        return ScrollDiscreteEvent(
            dx=libei.event_scroll_get_discrete_dx(self._cobject),
            dy=libei.event_scroll_get_discrete_dy(self._cobject),
        )

    @property
    def scroll_stop_event(self) -> ScrollStopEvent:
        return ScrollStopEvent(
            x=libei.event_scroll_get_stop_x(self._cobject),
            y=libei.event_scroll_get_stop_y(self._cobject),
        )

    @property
    def touch_event(self) -> TouchEvent:
        return TouchEvent(
            touchid=libei.event_touch_get_id(self._cobject),
            x=libei.event_touch_get_x(self._cobject),
            y=libei.event_touch_get_y(self._cobject),
        )


@ref_unref(libei)
class Seat(CObjectWrapper):
    def __init__(self, cobject):
        super().__init__(cobject)
        self._name = libei.seat_get_name(cobject).decode("utf-8")
        self._capabilities = tuple(
            c for c in DeviceCapability if libei.seat_has_capability(cobject, c)
        )

    def __str__(self) -> str:
        return (
            f"Seat: {self.name} {'|'.join(c.name for c in self.capabilities if c.name)}"
        )

    @property
    def name(self) -> str:
        """
        The seat name assigned by the EIS implementation.
        """
        return self._name

    @property
    def capabilities(self) -> tuple[DeviceCapability, ...]:
        """
        The set of capabilities available on this seat. The capabilities
        are constant for the lifetime of the seat and devices within this
        seat may only have one or more of these capabilities.

        Devices are not created by the EIS implementation until
        at least one capability is given in :meth:`bind`.
        """
        return self._capabilities

    def bind(self, capabilities: Optional[tuple[DeviceCapability]] = None):
        """
        Bind to the given set of capabilities. If capabilities is ``None``,
        all capabilities available on this seat are bound.

        This typically results in devices with any of the given
        capabilities to be created by the EIS implementation.
        """
        if capabilities is None:
            capabilities = self.capabilities

        libei.seat_bind_capabilities(self._cobject, *capabilities, 0)

    def unbind(self, capabilities: Optional[tuple[DeviceCapability]] = None):
        """
        Unbind the given set of capabilities. If capabilities is ``None``,
        all capabilities available on this seat are bound.

        This typically results in all devices with any of the given
        capabilities to be removed by the EIS implementation.
        """
        if capabilities is None:
            capabilities = self.capabilities
        libei.seat_unbind_capabilities(self._cobject, *capabilities, 0)


@ref_unref(libei)
class Device(CObjectWrapper):
    def __init__(self, cobject):
        super().__init__(cobject)
        self._seat = Seat.instance(libei.device_get_seat(self._cobject))
        self._name = libei.device_get_name(cobject).decode("utf-8")
        self._width = libei.device_get_width(cobject)
        self._height = libei.device_get_width(cobject)
        self._device_type = DeviceType(libei.device_get_type(cobject))
        self._capabilities = tuple(
            c for c in DeviceCapability if libei.device_has_capability(cobject, c)
        )

        def regions():
            idx = 0
            while True:
                region = libei.device_get_region(self._cobject, idx)
                if not region:
                    break
                yield region
                idx += 1

        self._regions = tuple(Region.instance(r) for r in regions())

    def __str__(self) -> str:
        caps = "|".join(c.name for c in self.capabilities if c.name)
        regions = "|".join(str(r) for r in self.regions)
        return f"Device: {self.name} <{self.device_type.name}> {caps} {regions}"

    @property
    def device_type(self) -> DeviceType:
        return self._device_type

    @property
    def name(self) -> str:
        """
        The name of this device as assigned by the EIS implementation.
        """
        return self._name

    @property
    def width(self) -> int:
        """
        The width of the device if the attr:`device_type` is
        :attr:`DeviceType.PHYSICAL`, otherwise zero
        """
        return self._width

    @property
    def height(self) -> int:
        """
        The height of the device if the attr:`device_type` is
        :attr:`DeviceType.PHYSICAL`, otherwise zero
        """
        return self._height

    @property
    def capabilities(self) -> tuple[DeviceCapability]:
        """
        The list of capabilities on this device. This is always a
        subset of the capabilities requested in :meth:`Seat.bind`.
        """
        return self._capabilities

    @property
    def regions(self) -> tuple["Region"]:
        """
        Return the set of regions available on this device. The set of regions
        is constant for the lifetime of the device.
        """
        return self._regions

    @property
    def seat(self) -> Seat:
        """
        Return the seat this device is attached to. The seat is constant
        for the lifetime of the device.
        """
        return self._seat

    def close(self):
        """
        Close this device and indicate that this client is no longer
        interested in it. This typically results in the device being
        removed from the seat, never to be seen again.
        """
        libei.device_close(self._cobject)

    def start_emulating(self, sequence: Optional[int] = None) -> "Device":
        """
        Start emulating events on this device. If the sequence is ``None``,
        a random (but monotonically increasing) number is assigned to this
        sequence.

        Returns ``self`` for easy chaining of events.
        """
        if sequence is None:
            sequence = int(time.time())
        libei.device_start_emulating(self._cobject, sequence)
        return self

    def stop_emulating(self) -> "Device":
        """
        Stop the current emulation sequence.

        Returns ``self`` for easy chaining of events.
        """
        libei.device_stop_emulating(self._cobject)
        return self

    def frame(self, timestamp: Optional[int] = None) -> "Device":
        """
        Send a frame event. If the timestamp is ``None`` is is replaced with
        the timestamp returned by :meth:`Context.now`.
        """
        if timestamp is None:
            timestamp = libei.now(libei.device_get_context(self._cobject))
        libei.device_frame(self._cobject, timestamp)
        return self

    def pointer_motion(self, dx: float, dy: float) -> "Device":
        """
        Send a relative motion event with the given delta. The deltas
        are in logical pixels or mm, depending on the :attr:`device_type`.

        This function does nothing if the device is not currently
        emulating.

        Returns ``self`` for easy chaining of events.
        """
        libei.device_pointer_motion(self._cobject, dx, dy)
        return self

    def pointer_motion_absolute(self, x: float, y: float) -> "Device":
        """
        Send a absolute motion event with the given position. The position
        is in logical pixels or mm, depending on the :attr:`device_type`.

        This function does nothing if the device is not currently
        emulating.

        Returns ``self`` for easy chaining of events.
        """
        libei.device_pointer_motion_absolute(self._cobject, x, y)
        return self

    def button_button(self, button: int, is_press: bool) -> "Device":
        """
        Send a button event. The button number must be one of
        those defined in ``linux/input-event-codes.h``.

        This function does nothing if the device is not currently
        emulating.

        Returns ``self`` for easy chaining of events.
        """
        libei.device_button_button(self._cobject, button, is_press)
        return self

    def keyboard_key(self, key: int, is_press: bool) -> "Device":
        """
        Send a key event. The key must be one of
        those defined in ``linux/input-event-codes.h``.

        This function does nothing if the device is not currently
        emulating.

        Returns ``self`` for easy chaining of events.
        """
        libei.device_keyboard_key(self._cobject, key, is_press)
        return self

    def touch_new(self) -> "Touch":
        """
        Create a new touch event. This touch does nothing until
        :meth:`Touch.down` is called.

        This function does nothing if the device is not currently
        emulating.

        Returns ``self`` for easy chaining of events.
        """
        return Touch.instance(libei.device_touch_new(self._cobject))


@dataclasses.dataclass(eq=True)
class Position:
    x: int
    y: int


@dataclasses.dataclass(eq=True)
class Dimension:
    width: int
    height: int


@ref_unref(libei)
class Region(CObjectWrapper):
    def __init__(self, cobject):
        super().__init__(cobject)
        self._position = Position(
            libei.region_get_x(self._cobject), libei.region_get_y(self._cobject)
        )
        self._dimension = Dimension(
            libei.region_get_width(self._cobject),
            libei.region_get_height(self._cobject),
        )
        self._scale = libei.region_get_physical_scale(self._cobject)
        try:
            mid = libei.region_get_mapping_id(self._cobject)
            self._mapping_id = mid.decode("utf-8") if mid else None
        except NotImplementedError:
            self._mapping_id = None

    def __str__(self) -> str:
        return f"{self.dimension.width}x{self.dimension.height}@{self.position.x},{self.position.y}:s{self.physical_scale}|mid<{self.mapping_id}>"

    @property
    def position(self) -> Position:
        return self._position

    @property
    def dimension(self) -> Dimension:
        return self._dimension

    @property
    def physical_scale(self) -> float:
        return self._scale

    @property
    def mapping_id(self) -> Optional[str]:
        return self._mapping_id

    def contains(self, x: float, y: float) -> bool:
        """
        Return ``True`` if the given position is within this region,
        or ``False`` otherwise.

        For example, a region ``640x480@100,100`` returns ``True``
        for the position ``100/100`` and ``False`` for
        the position ``640/580``.
        """
        return libei.region_contains(self._cobject, x, y)

    def convert(self, x: float, y: float) -> Union[tuple[float, float], tuple[()]]:
        """
        Convert the given position to a position relative to this
        region's origin and return the updated ``(x, y)`` tuple.

        If the position is outside this region, the empty tuple is returned
        instead.

        For example, a region ``640x480@100,100``
        converts the position ``100/150`` to ``0/50``.
        """
        import ctypes

        x_out = ctypes.c_double(x)
        print(x_out)
        y_out = ctypes.c_double(y)
        in_region = libei.region_convert_point(
            self._cobject, ctypes.byref(x_out), ctypes.byref(y_out)
        )
        return (x_out.value, y_out.value) if in_region else ()


@ref_unref(libei)
class Keymap(CObjectWrapper):
    def __init__(self, cobject):
        super().__init__(cobject)
        self._keymap_type = KeymapType(libei.keymap_get_type(cobject))
        self._size = libei.keymap_get_size(cobject)
        self._fd = os.fdopen(libei.keymap_get_fd(cobject))

    @property
    def keymap_type(self) -> KeymapType:
        return self._keymap_type

    @property
    def device(self) -> Device:
        return Device.instance(libei.keymap_get_device(self._cobject))

    @property
    def size(self) -> int:
        return self._size

    @property
    def fd(self) -> IO:
        return self._fd


@unref(libei)
class Touch(CObjectWrapper):
    @property
    def device(self) -> Device:
        return Device.instance(libei.touch_get_device(self._cobject))

    def down(self, x: float, y: float) -> "Device":
        """
        Logically put the touch down at the given position. The position
        is in logical pixels or mm, depending on the :attr:`device_type`.

        This function does nothing if the device is not currently
        emulating.

        Returns the touch's :attr:`device` for easy chaining of events.
        """
        libei.touch_down(self._cobject, x, y)
        return self.device

    def motion(self, x: float, y: float) -> "Device":
        """
        Logically move the touch to the given position. The position
        is in logical pixels or mm, depending on the :attr:`device_type`.

        This function does nothing if the device is not currently
        emulating.

        Returns the touch's :attr:`device` for easy chaining of events.
        """
        libei.touch_motion(self._cobject, x, y)
        return self.device

    def up(self) -> "Device":
        """
        Logically lift the touch.

        This function does nothing if the device is not currently
        emulating.

        Returns the touch's :attr:`device` for easy chaining of events.
        """
        libei.touch_up(self._cobject)
        return self.device


@CFUNCTYPE(None, c_void_p, c_int, c_char_p, c_void_p)
def loghandler(ei_context, priority, message, log_context):
    level = {
        _LogPriority.DEBUG: logging.DEBUG,
        _LogPriority.INFO: logging.INFO,
        _LogPriority.WARNING: logging.WARNING,
        _LogPriority.ERROR: logging.ERROR,
    }.get(priority, logging.DEBUG)
    logger.log(level, message.decode("utf-8"))


@unref(libei, unref_func="unref")
class Context(CObjectWrapper):
    """
    Represents a libei context. Use :class:`Sender` or :class:`Receiver` to create
    a context.

    Logging of the libei context is hooked into Python's ``logging`` module
    as the ``ei`` logger.

    .. warning:: This context is refcounted and automatically destroys the underlying C object
                 when the reference is dropped. This may cause all sub-objects in the underlying C
                 object to be destroyed.
    """

    def __init__(self, cobject, name: str):
        super().__init__(cobject)
        self._name = name
        self._fd = os.fdopen(libei.get_fd(self._cobject))
        self._backend_fd = None
        libei.log_set_handler(self._cobject, loghandler)
        libei.log_set_priority(self._cobject, _LogPriority.DEBUG)
        libei.configure_name(self._cobject, name.encode("utf-8"))

    @property
    def name(self) -> str:
        """
        This EI client's name. Note that the name may or may not be honored by
        the EIS implementation. For example in the case of a client going through
        an XDG Desktop Portal, the app-id may be used instead.
        """
        return self._name

    @property
    def fd(self) -> IO:
        """
        Return the file descriptor to monitor (e.g. using ``select``).
        When this file descriptor has events pending, immediately call :meth:`dispatch`.
        """
        return self._fd

    @property
    def events(self) -> Iterator[Event]:
        """
        Return an iterator over the currently pending events.
        The events will be automatically cleaned up when the
        references are dropped. This can cause subtle bugs,
        in particular this example code never prints events: ::

            if len(ei.events) > 0:   # consumes and discards all events
                for e in ei.events:  # always [] because no events are waiting
                    print(e)

        The events returned by the first call to `ei.events` are
        iterated on and cleaned up by `len()` - on the second call
        to `ei.events` no further events are pending.

        Do this instead: ::

            events = ei.events
            if len(events) > 0:
                for e in events:
                    print(e)
        """
        while True:
            e = libei.get_event(self._cobject)
            if not e:
                break
            yield Event(e)

    @property
    def now(self) -> int:
        """
        Return a ``CLOCK_MONOTONIC`` timestamp in microseconds
        """
        return libei.now(self._cobject)

    def _set_fd(self, fd: IO) -> "Context":
        err = libei.setup_backend_fd(self._cobject, fd.fileno())
        if err < 0:
            raise ConnectionError(os.strerror(-err), -err)
        # Keep a copy of the fd because libei doesn't dup(). Without this
        # our connection will be closed when the caller's fd is closed
        self._backend_fd = fd
        return self

    def _set_socket(self, path: Optional[Path]) -> "Context":
        full_path: Optional[bytes] = os.fspath(path).encode("utf-8") if path else None
        err = libei.setup_backend_socket(self._cobject, full_path)
        if err < 0:
            raise ConnectionError(os.strerror(-err), -err)
        return self

    def dispatch(self) -> None:
        """
        Dispatch internally pending events. This method must be called immediately
        after :attr:`fd` has input pending. After calling :meth:`dispatch`, events
        **may** be available in :attr:`events`.
        """
        libei.dispatch(self._cobject)


class Receiver(Context):
    def __init__(self, name: str):
        cobject = libei.new_receiver(None)
        super().__init__(cobject, name)

    @classmethod
    def create_for_fd(cls, fd: IO, name: Optional[str] = None):
        """
        Create a Receiver EI context for the given file descriptor.
        This fd must be connected to an EIS implementation ready
        to accept clients or a :class:`ConnectionError` is raised.

        If name is ``None`` a default name will be used instead.
        """
        ctx = cls(name=name or "unnamed client")._set_fd(fd)
        return ctx

    @classmethod
    def create_for_socket(cls, path: Optional[Path], name: Optional[str] = None):
        """
        Create a Receiver EI context for an EIS implementation on
        the given socket path. If the path is ``None`` the environment
        variable ``LIBEI_SOCKET`` is used by the underlying library.

        The socket path must exist and be ready to accept clients or
        a :class:`ConnectionError` is raised.

        If name is ``None`` a default name will be used instead.
        """
        ctx = cls(name=name or "unnamed client")._set_socket(path)
        return ctx


class Sender(Context):
    def __init__(self, name: str):
        cobject = libei.new_sender(None)
        super().__init__(cobject, name)

    @classmethod
    def create_for_fd(cls, fd: IO, name: Optional[str] = None):
        """
        Create a Sender EI context for the given file descriptor.
        This fd must be connected to an EIS implementation ready
        to accept clients or a :class:`ConnectionError` is raised.

        If name is ``None`` a default name will be used instead.
        """
        ctx = cls(name=name or "unnamed client")._set_fd(fd)
        return ctx

    @classmethod
    def create_for_socket(cls, path: Optional[Path], name: Optional[str] = None):
        """
        Create a Sender EI context for an EIS implementation on
        the given socket path. If the path is ``None`` the environment
        variable ``LIBEI_SOCKET`` is used by the underlying library.

        The socket path must exist and be ready to accept clients or
        a :class:`ConnectionError` is raised.

        If name is ``None`` a default name will be used instead.
        """
        ctx = cls(name=name or "unnamed client")._set_socket(path)
        return ctx
