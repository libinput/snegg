# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black
from ctypes import c_void_p
import weakref


def unref(lib, unref_func=None):
    """
    Class decorator for an object that needs an unref function to be called on
    destruction.

    The actual handling of the unref is done by the :class:`CObjectWrapper` class,
    the decorator has no effect otherwise.
    """

    def wrap(cls):
        cls._unref_function = unref_func or f"{cls.__name__.lower()}_unref"
        cls._unref_cdll = lib
        return cls

    return wrap


def ref_unref(lib, ref_func=None, unref_func=None):
    """
    Class decorator for an object that needs a ref function to be called on
    creation and the unref function on destruction.

    The actual handling of the ref/unref is done by the :class:`CObjectWrapper` class,
    the decorator has no effect otherwise.
    """

    def wrap(cls):
        cls._ref_function = ref_func or f"{cls.__name__.lower()}_ref"
        cls._unref_function = unref_func or f"{cls.__name__.lower()}_unref"
        cls._ref_cdll = lib
        cls._unref_cdll = lib
        return cls

    return wrap


class CObjectWrapper:
    """
    Parent class for an object that's a representation of a
    C opaque object.
    """

    _objects: weakref.WeakValueDictionary[
        c_void_p, "CObjectWrapper"
    ] = weakref.WeakValueDictionary()
    """
    All C pointers are kept in the above _objects lookup table.
    This is a weakref dictionary so the dict doesn't keep the objects alive.

    When lookup is called, we either return the existing object for
    the C pointer value or create it only the fly.
    """

    def __init__(self, cobj):
        self._cobject = cobj
        ref_func = getattr(self, "_ref_function", None)
        ref_lib = getattr(self, "_ref_cdll", None)
        if ref_func and ref_lib:
            func = getattr(ref_lib, ref_func)
            func(cobj)

        unref_func = getattr(self, "_unref_function", None)
        unref_lib = getattr(self, "_unref_cdll", None)
        if unref_func and unref_lib:
            func = getattr(unref_lib, unref_func)
            weakref.finalize(self, func, cobj)

    @classmethod
    def instance(cls, cobj: c_void_p):
        """
        Return the existing object with the given pointer value,
        if it exists, otherwise create it on the fly, add it to
        the LUT and return it.
        """
        obj = cls._objects.get(cobj)
        if obj:
            assert isinstance(
                obj, cls
            ), f"C pointer already stored as object {obj} ({type(obj)})"
        else:
            obj = cls(cobj)
            cls._objects[cobj] = obj
        return obj
