# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

from . import clibwrapper, cfunc
from ctypes import c_void_p


# fmt: off
@clibwrapper(soname="libei.so.1")
class LibEi:
    log_context_get_line = cfunc("unsigned int ei_log_context_get_line(struct ei_log_context *ctx);")
    log_context_get_file = cfunc("const char * ei_log_context_get_file(struct ei_log_context *ctx);")
    log_context_get_func = cfunc("const char * ei_log_context_get_func(struct ei_log_context *ctx);")
    log_set_handler = cfunc("void ei_log_set_handler(struct ei *ei, ei_log_handler log_handler);", typemap = {"ei_log_handler": c_void_p})
    log_set_priority = cfunc("void ei_log_set_priority(struct ei *ei, enum ei_log_priority priority);")
    log_get_priority = cfunc("enum ei_log_priority ei_log_get_priority(const struct ei *ei);")
    event_type_to_string = cfunc("const char* ei_event_type_to_string(enum ei_event_type);")
    new_sender = cfunc("struct ei *ei_new_sender(void *userdata);")
    new_receiver = cfunc("struct ei *ei_new_receiver(void *userdata);")
    unref = cfunc("struct ei *ei_unref(struct ei *ei);")
    configure_name = cfunc("void ei_configure_name(struct ei * ei, const char *name);")
    setup_backend_fd = cfunc("int ei_setup_backend_fd(struct ei *ei, int fd);")
    setup_backend_socket = cfunc("int ei_setup_backend_socket(struct ei *ei, const char *path);")
    get_fd = cfunc("int ei_get_fd(struct ei *ei);")
    dispatch = cfunc("void ei_dispatch(struct ei *ei);")
    get_event = cfunc("struct ei_event * ei_get_event(struct ei *ei);")
    peek_event = cfunc("struct ei_event * ei_peek_event(struct ei *ei);")
    now = cfunc("uint64_t ei_now(struct ei *ei);")
    seat_get_name = cfunc("const char * ei_seat_get_name(struct ei_seat *seat);")
    seat_has_capability = cfunc("bool ei_seat_has_capability(struct ei_seat *seat, enum ei_device_capability cap);")
    seat_bind_capabilities = cfunc("void ei_seat_bind_capabilities(struct ei_seat *seat, ...);")
    seat_unbind_capabilities = cfunc("void ei_seat_unbind_capabilities(struct ei_seat *seat, ...)")
    seat_ref = cfunc("struct ei_seat * ei_seat_ref(struct ei_seat *seat);")
    seat_unref = cfunc("struct ei_seat * ei_seat_unref(struct ei_seat *seat);")
    event_unref = cfunc("struct ei_event * ei_event_unref(struct ei_event *event);")
    event_get_type = cfunc("enum ei_event_type ei_event_get_type(struct ei_event *event);")
    event_get_seat = cfunc("struct ei_seat * ei_event_get_seat(struct ei_event *event);")
    event_get_device = cfunc("struct ei_device * ei_event_get_device(struct ei_event *event);")
    event_get_time = cfunc("uint64_t ei_event_get_time(struct ei_event *event);")
    device_ref = cfunc("struct ei_device * ei_device_ref(struct ei_device *device);")
    device_unref = cfunc("struct ei_device * ei_device_unref(struct ei_device *device);")
    device_get_seat = cfunc("struct ei_seat * ei_device_get_seat(struct ei_device *device);")
    device_get_width = cfunc("uint32_t ei_device_get_width(struct ei_device *device);")
    device_get_height = cfunc("uint32_t ei_device_get_height(struct ei_device *device);")
    device_keyboard_get_keymap = cfunc("struct ei_keymap * ei_device_keyboard_get_keymap(struct ei_device *device);")
    device_close = cfunc("void ei_device_close(struct ei_device *device);")
    device_get_name = cfunc("const char * ei_device_get_name(struct ei_device *device);")
    device_get_type = cfunc("enum ei_device_type ei_device_get_type(struct ei_device *device);")
    device_has_capability = cfunc("bool ei_device_has_capability(struct ei_device *device, enum ei_device_capability cap);")
    device_get_region = cfunc("struct ei_region * ei_device_get_region(struct ei_device *device, size_t index);")
    device_get_region_at = cfunc("struct ei_region * ei_device_get_region_at(struct ei_device *device, double x, double y);")
    device_get_context = cfunc("struct ei * ei_device_get_context(struct ei_device *device);")
    keymap_get_size = cfunc("size_t ei_keymap_get_size(struct ei_keymap *keymap);")
    keymap_get_type = cfunc("enum ei_keymap_type ei_keymap_get_type(struct ei_keymap *keymap);")
    keymap_get_fd = cfunc("int ei_keymap_get_fd(struct ei_keymap *keymap);")
    keymap_get_device = cfunc("struct ei_device * ei_keymap_get_device(struct ei_keymap *keymap);")
    keymap_ref = cfunc("struct ei_keymap * ei_keymap_ref(struct ei_keymap *keymap);")
    keymap_unref = cfunc("struct ei_keymap * ei_keymap_unref(struct ei_keymap *keymap);")
    region_ref = cfunc("struct ei_region * ei_region_ref(struct ei_region *region);")
    region_unref = cfunc("struct ei_region * ei_region_unref(struct ei_region *region);")
    region_get_x = cfunc("uint32_t ei_region_get_x(struct ei_region *region);")
    region_get_y = cfunc("uint32_t ei_region_get_y(struct ei_region *region);")
    region_get_width = cfunc("uint32_t ei_region_get_width(struct ei_region *region);")
    region_get_height = cfunc("uint32_t ei_region_get_height(struct ei_region *region);")
    region_contains = cfunc("bool ei_region_contains(struct ei_region *region, double x, double y);")
    region_convert_point = cfunc("bool ei_region_convert_point(struct ei_region *region, double *x, double *y);")
    region_get_physical_scale = cfunc("double ei_region_get_physical_scale(struct ei_region *region);")
    region_get_mapping_id = cfunc("const char *ei_region_get_mapping_id(struct ei_region *region);")
    device_start_emulating = cfunc("void ei_device_start_emulating(struct ei_device *device, uint32_t sequence);")
    device_stop_emulating = cfunc("void ei_device_stop_emulating(struct ei_device *device);")
    device_frame = cfunc("void ei_device_frame(struct ei_device *device, uint64_t time);")
    device_pointer_motion = cfunc("void ei_device_pointer_motion(struct ei_device *device, double x, double y);")
    device_pointer_motion_absolute = cfunc("void ei_device_pointer_motion_absolute(struct ei_device *device, double x, double y);")
    device_button_button = cfunc("void ei_device_button_button(struct ei_device *device, uint32_t button, bool is_press);")
    device_scroll_delta = cfunc("void ei_device_scroll_delta(struct ei_device *device, double x, double y);")
    device_scroll_discrete = cfunc("void ei_device_scroll_discrete(struct ei_device *device, int32_t x, int32_t y);")
    device_scroll_stop = cfunc("void ei_device_scroll_stop(struct ei_device *device, bool stop_x, bool stop_y);")
    device_scroll_cancel = cfunc("void ei_device_scroll_cancel(struct ei_device *device, bool cancel_x, bool cancel_y);")
    device_keyboard_key = cfunc("void ei_device_keyboard_key(struct ei_device *device, uint32_t keycode, bool is_press);")
    device_touch_new = cfunc("struct ei_touch * ei_device_touch_new(struct ei_device *device);")
    event_emulating_get_sequence = cfunc("uint32_t ei_event_emulating_get_sequence(struct ei_event *event);")
    event_keyboard_get_xkb_mods_depressed = cfunc("uint32_t ei_event_keyboard_get_xkb_mods_depressed(struct ei_event *event);")
    event_keyboard_get_xkb_mods_latched = cfunc("uint32_t ei_event_keyboard_get_xkb_mods_latched(struct ei_event *event);")
    event_keyboard_get_xkb_mods_locked = cfunc("uint32_t ei_event_keyboard_get_xkb_mods_locked(struct ei_event *event);")
    event_keyboard_get_xkb_group = cfunc("uint32_t ei_event_keyboard_get_xkb_group(struct ei_event *event);")
    event_pointer_get_dx = cfunc("double ei_event_pointer_get_dx(struct ei_event *event);")
    event_pointer_get_dy = cfunc("double ei_event_pointer_get_dy(struct ei_event *event);")
    event_pointer_get_absolute_x = cfunc("double ei_event_pointer_get_absolute_x(struct ei_event *event);")
    event_pointer_get_absolute_y = cfunc("double ei_event_pointer_get_absolute_y(struct ei_event *event);")
    event_button_get_button = cfunc("uint32_t ei_event_button_get_button(struct ei_event *event);")
    event_button_get_is_press = cfunc("bool ei_event_button_get_is_press(struct ei_event *event);")
    event_scroll_get_dx = cfunc("double ei_event_scroll_get_dx(struct ei_event *event);")
    event_scroll_get_dy = cfunc("double ei_event_scroll_get_dy(struct ei_event *event);")
    event_scroll_get_stop_x = cfunc("bool ei_event_scroll_get_stop_x(struct ei_event *event);")
    event_scroll_get_stop_y = cfunc("bool ei_event_scroll_get_stop_y(struct ei_event *event);")
    event_scroll_get_discrete_dx = cfunc("int32_t ei_event_scroll_get_discrete_dx(struct ei_event *event);")
    event_scroll_get_discrete_dy = cfunc("int32_t ei_event_scroll_get_discrete_dy(struct ei_event *event);")
    event_keyboard_get_key = cfunc("uint32_t ei_event_keyboard_get_key(struct ei_event *event);")
    event_keyboard_get_key_is_press = cfunc("bool ei_event_keyboard_get_key_is_press(struct ei_event *event);")
    event_touch_get_id = cfunc("uint32_t ei_event_touch_get_id(struct ei_event *event);")
    event_touch_get_x = cfunc("double ei_event_touch_get_x(struct ei_event *event);")
    event_touch_get_y = cfunc("double ei_event_touch_get_y(struct ei_event *event);")
    touch_ref = cfunc("struct ei_touch * ei_touch_ref(struct ei_touch *touch);")
    touch_unref = cfunc("struct ei_touch * ei_touch_unref(struct ei_touch *touch);")
    touch_down = cfunc("void ei_touch_down(struct ei_touch *touch, double x, double y);")
    touch_motion = cfunc("void ei_touch_motion(struct ei_touch *touch, double x, double y);")
    touch_up = cfunc("void ei_touch_up(struct ei_touch *touch);")
    touch_get_device = cfunc("struct ei_device * ei_touch_get_device(struct ei_touch *touch);")
# fmt: on


libei = LibEi()
