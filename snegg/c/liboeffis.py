# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

from . import clibwrapper, cfunc


# fmt: off
@clibwrapper(soname="liboeffis.so.1")
class LibOeffis:
    new = cfunc("struct oeffis * oeffis_new(void *user_data)")
    ref = cfunc("struct oeffis * oeffis_ref(struct oeffis *oeffis)")
    unref = cfunc("struct oeffis * oeffis_unref(struct oeffis *oeffis)")
    get_fd = cfunc("int oeffis_get_fd(struct oeffis *oeffis)")
    get_eis_fd = cfunc("int oeffis_get_eis_fd(struct oeffis *oeffis)")
    create_session = cfunc("void oeffis_create_session(struct oeffis *oeffis, uint32_t devices)")
    create_session_on_bus = cfunc("void oeffis_create_session_on_bus(struct oeffis *oeffis, const char *busname, uint32_t devices)")
    dispatch = cfunc("void oeffis_dispatch(struct oeffis *oeffis)")
    get_event = cfunc("enum oeffis_event_type oeffis_get_event(struct oeffis *oeffis)")
    get_error_message = cfunc("const char * oeffis_get_error_message(struct oeffis *oeffis)")
# fmt: on


liboeffis = LibOeffis()
