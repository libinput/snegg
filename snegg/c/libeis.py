# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

from . import clibwrapper, cfunc
from ctypes import c_void_p


# fmt: off
@clibwrapper(soname="libeis.so.1")
class LibEis:
    log_context_get_line = cfunc("unsigned int eis_log_context_get_line(struct eis_log_context *ctx);")
    log_context_get_file = cfunc("const char * eis_log_context_get_file(struct eis_log_context *ctx);")
    log_context_get_func = cfunc("const char * eis_log_context_get_func(struct eis_log_context *ctx);")
    log_set_handler = cfunc("void eis_log_set_handler(struct eis *eis, eis_log_handler log_handler);", typemap = {"eis_log_handler": c_void_p})
    log_set_priority = cfunc("void eis_log_set_priority(struct eis *eis, enum eis_log_priority priority);")
    log_get_priority = cfunc("enum eis_log_priority eis_log_get_priority(const struct eis *eis);")
    event_type_to_string = cfunc("const char* eis_event_type_to_string(enum eis_event_type);")
    new = cfunc("struct eis * eis_new(void *user_data);")
    unref = cfunc("struct eis * eis_unref(struct eis *eis);")
    setup_backend_socket = cfunc("int eis_setup_backend_socket(struct eis *ctx, const char *path);")
    setup_backend_fd = cfunc("int eis_setup_backend_fd(struct eis *ctx);")
    backend_fd_add_client = cfunc("int eis_backend_fd_add_client(struct eis *ctx);")
    get_fd = cfunc("int eis_get_fd(struct eis *eis);")
    dispatch = cfunc("void eis_dispatch(struct eis *eis);")
    get_event = cfunc("struct eis_event * eis_get_event(struct eis *eis);")
    peek_event = cfunc("struct eis_event * eis_peek_event(struct eis *eis);")
    event_unref = cfunc("struct eis_event * eis_event_unref(struct eis_event *event);")
    event_seat_has_capability = cfunc("bool eis_event_seat_has_capability(struct eis_event *event, enum eis_device_capability cap);")
    event_get_type = cfunc("enum eis_event_type eis_event_get_type(struct eis_event *event);")
    event_get_client = cfunc("struct eis_client * eis_event_get_client(struct eis_event *event);")
    event_get_seat = cfunc("struct eis_seat * eis_event_get_seat(struct eis_event *event);")
    event_get_device = cfunc("struct eis_device * eis_event_get_device(struct eis_event *event);")
    event_get_time = cfunc("uint64_t eis_event_get_time(struct eis_event *event);")
    client_ref = cfunc("struct eis_client * eis_client_ref(struct eis_client *client);")
    client_unref = cfunc("struct eis_client * eis_client_unref(struct eis_client *client);")
    client_is_sender = cfunc("bool eis_client_is_sender(struct eis_client *client);")
    client_get_name = cfunc("const char * eis_client_get_name(struct eis_client *client);")
    client_connect = cfunc("void eis_client_connect(struct eis_client *client);")
    client_disconnect = cfunc("void eis_client_disconnect(struct eis_client *client);")
    client_new_seat = cfunc("struct eis_seat * eis_client_new_seat(struct eis_client *client, const char *name);")
    seat_ref = cfunc("struct eis_seat * eis_seat_ref(struct eis_seat *seat);")
    seat_unref = cfunc("struct eis_seat * eis_seat_unref(struct eis_seat *seat);")
    seat_get_client = cfunc("struct eis_client * eis_seat_get_client(struct eis_seat *eis_seat);")
    seat_get_name = cfunc("const char * eis_seat_get_name(struct eis_seat *eis_seat);")
    seat_has_capability = cfunc("bool eis_seat_has_capability(struct eis_seat *seat, enum eis_device_capability cap);")
    seat_configure_capability = cfunc("void eis_seat_configure_capability(struct eis_seat *seat, enum eis_device_capability cap);")
    seat_add = cfunc("void eis_seat_add(struct eis_seat *seat);")
    seat_remove = cfunc("void eis_seat_remove(struct eis_seat *seat);")
    device_get_context = cfunc("struct eis * eis_device_get_context(struct eis_device *device);")
    device_get_client = cfunc("struct eis_client * eis_device_get_client(struct eis_device *device);")
    device_get_seat = cfunc("struct eis_seat * eis_device_get_seat(struct eis_device *device);")
    device_ref = cfunc("struct eis_device * eis_device_ref(struct eis_device *device);")
    device_unref = cfunc("struct eis_device * eis_device_unref(struct eis_device *device);")
    device_get_name = cfunc("const char * eis_device_get_name(struct eis_device *device);")
    device_has_capability = cfunc("bool eis_device_has_capability(struct eis_device *device, enum eis_device_capability cap);")
    device_get_width = cfunc("uint32_t eis_device_get_width(struct eis_device *device);")
    device_get_height = cfunc("uint32_t eis_device_get_height(struct eis_device *device);")
    seat_new_device = cfunc("struct eis_device * eis_seat_new_device(struct eis_seat *seat);")
    device_configure_type = cfunc("void eis_device_configure_type(struct eis_device *device, enum eis_device_type type);")
    device_get_type = cfunc("enum eis_device_type eis_device_get_type(struct eis_device *device);")
    device_configure_name = cfunc("void eis_device_configure_name(struct eis_device *device, const char *name);")
    device_configure_capability = cfunc("void eis_device_configure_capability(struct eis_device *device, enum eis_device_capability cap);")
    device_configure_size = cfunc("void eis_device_configure_size(struct eis_device *device, uint32_t width, uint32_t height);")
    device_new_region = cfunc("struct eis_region * eis_device_new_region(struct eis_device *device);")
    region_contains = cfunc("bool eis_region_contains(struct eis_region *region, double x, double y);")
    region_get_physical_scale = cfunc("double eis_region_get_physical_scale(struct eis_region *region);")
    region_set_size = cfunc("void eis_region_set_size(struct eis_region *region, uint32_t w, uint32_t h);")
    region_set_offset = cfunc("void eis_region_set_offset(struct eis_region *region, uint32_t x, uint32_t y);")
    region_set_physical_scale = cfunc("void eis_region_set_physical_scale(struct eis_region *region, double scale);")
    region_set_mapping_id = cfunc("void eis_region_set_mapping_id(struct eis_region *region, const char *mapping_id);")
    region_get_mapping_id = cfunc("const char* eis_region_get_mapping_id(struct eis_region *region);")
    region_add = cfunc("void eis_region_add(struct eis_region *region);")
    device_get_region = cfunc("struct eis_region * eis_device_get_region(struct eis_device *device, size_t index);")
    device_get_region_at = cfunc("struct eis_region * eis_device_get_region_at(struct eis_device *device, double x, double y);")
    region_ref = cfunc("struct eis_region * eis_region_ref(struct eis_region *region);")
    region_unref = cfunc("struct eis_region * eis_region_unref(struct eis_region *region);")
    region_get_x = cfunc("uint32_t eis_region_get_x(struct eis_region *region);")
    region_get_y = cfunc("uint32_t eis_region_get_y(struct eis_region *region);")
    region_get_width = cfunc("uint32_t eis_region_get_width(struct eis_region *region);")
    region_get_height = cfunc("uint32_t eis_region_get_height(struct eis_region *region);")
    device_add = cfunc("void eis_device_add(struct eis_device *device);")
    device_remove = cfunc("void eis_device_remove(struct eis_device *device);")
    device_pause = cfunc("void eis_device_pause(struct eis_device *device);")
    device_resume = cfunc("void eis_device_resume(struct eis_device *device);")
    device_new_keymap = cfunc("struct eis_keymap * eis_device_new_keymap(struct eis_device *device, enum eis_keymap_type type, int fd, size_t size);")
    keymap_add = cfunc("void eis_keymap_add(struct eis_keymap *keymap);")
    keymap_get_size = cfunc("size_t eis_keymap_get_size(struct eis_keymap *keymap);")
    keymap_get_type = cfunc("enum eis_keymap_type eis_keymap_get_type(struct eis_keymap *keymap);")
    keymap_get_fd = cfunc("int eis_keymap_get_fd(struct eis_keymap *keymap);")
    keymap_ref = cfunc("struct eis_keymap * eis_keymap_ref(struct eis_keymap *keymap);")
    keymap_unref = cfunc("struct eis_keymap * eis_keymap_unref(struct eis_keymap *keymap);")
    keymap_get_device = cfunc("struct eis_device * eis_keymap_get_device(struct eis_keymap *keymap);")
    device_keyboard_get_keymap = cfunc("struct eis_keymap * eis_device_keyboard_get_keymap(struct eis_device *device);")
    device_keyboard_send_xkb_modifiers = cfunc("void eis_device_keyboard_send_xkb_modifiers(struct eis_device *device, uint32_t depressed, uint32_t latched, uint32_t locked, uint32_t group);")
    device_start_emulating = cfunc("void eis_device_start_emulating(struct eis_device *device, uint32_t sequence);")
    device_stop_emulating = cfunc("void eis_device_stop_emulating(struct eis_device *device);")
    device_frame = cfunc("void eis_device_frame(struct eis_device *device, uint64_t time);")
    device_pointer_motion = cfunc("void eis_device_pointer_motion(struct eis_device *device, double x, double y);")
    device_pointer_motion_absolute = cfunc("void eis_device_pointer_motion_absolute(struct eis_device *device, double x, double y);")
    device_button_button = cfunc("void eis_device_button_button(struct eis_device *device, uint32_t button, bool is_press);")
    device_scroll_delta = cfunc("void eis_device_scroll_delta(struct eis_device *device, double x, double y);")
    device_scroll_discrete = cfunc("void eis_device_scroll_discrete(struct eis_device *device, int32_t x, int32_t y);")
    device_scroll_stop = cfunc("void eis_device_scroll_stop(struct eis_device *device, bool stop_x, bool stop_y);")
    device_scroll_cancel = cfunc("void eis_device_scroll_cancel(struct eis_device *device, bool cancel_x, bool cancel_y);")
    device_keyboard_key = cfunc("void eis_device_keyboard_key(struct eis_device *device, uint32_t keycode, bool is_press);")
    device_touch_new = cfunc("struct eis_touch * eis_device_touch_new(struct eis_device *device);")
    touch_down = cfunc("void eis_touch_down(struct eis_touch *touch, double x, double y);")
    touch_motion = cfunc("void eis_touch_motion(struct eis_touch *touch, double x, double y);")
    touch_up = cfunc("void eis_touch_up(struct eis_touch *touch);")
    touch_ref = cfunc("struct eis_touch * eis_touch_ref(struct eis_touch *touch);")
    touch_unref = cfunc("struct eis_touch * eis_touch_unref(struct eis_touch *touch);")
    touch_get_device = cfunc("struct eis_device * eis_touch_get_device(struct eis_touch *touch);")
    event_emulating_get_sequence = cfunc("uint32_t eis_event_emulating_get_sequence(struct eis_event *event);")
    event_pointer_get_dx = cfunc("double eis_event_pointer_get_dx(struct eis_event *event);")
    event_pointer_get_dy = cfunc("double eis_event_pointer_get_dy(struct eis_event *event);")
    event_pointer_get_absolute_x = cfunc("double eis_event_pointer_get_absolute_x(struct eis_event *event);")
    event_pointer_get_absolute_y = cfunc("double eis_event_pointer_get_absolute_y(struct eis_event *event);")
    event_button_get_button = cfunc("uint32_t eis_event_button_get_button(struct eis_event *event);")
    event_button_get_is_press = cfunc("bool eis_event_button_get_is_press(struct eis_event *event);")
    event_scroll_get_dx = cfunc("double eis_event_scroll_get_dx(struct eis_event *event);")
    event_scroll_get_dy = cfunc("double eis_event_scroll_get_dy(struct eis_event *event);")
    event_scroll_get_stop_x = cfunc("bool eis_event_scroll_get_stop_x(struct eis_event *event);")
    event_scroll_get_stop_y = cfunc("bool eis_event_scroll_get_stop_y(struct eis_event *event);")
    event_scroll_get_discrete_dx = cfunc("int32_t eis_event_scroll_get_discrete_dx(struct eis_event *event);")
    event_scroll_get_discrete_dy = cfunc("int32_t eis_event_scroll_get_discrete_dy(struct eis_event *event);")
    event_keyboard_get_key = cfunc("uint32_t eis_event_keyboard_get_key(struct eis_event *event);")
    event_keyboard_get_key_is_press = cfunc("bool eis_event_keyboard_get_key_is_press(struct eis_event *event);")
    event_touch_get_id = cfunc("uint32_t eis_event_touch_get_id(struct eis_event *event);")
    event_touch_get_x = cfunc("double eis_event_touch_get_x(struct eis_event *event);")
    event_touch_get_y = cfunc("double eis_event_touch_get_y(struct eis_event *event);")
    now = cfunc("uint64_t eis_now(struct eis *ei);")
# fmt: on

libeis = LibEis()
