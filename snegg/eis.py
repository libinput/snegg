# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

"""
Wrapper module around the ``libeis`` C library. This is a thin API wrapper with
most of the semantics of the underlying C library preserved.
See the `libeis documentation <https://libinput.pages.freedesktop.org/libei/api/group__libeis.html>`_
for details on each API.

.. warning:: Most objects in this module are refcounted and automatically destroy
             the underlying C object when the Python reference is dropped.
             This may cause all sub-objects in the underlying C object to be
             destroyed as well. Care must be taken to preserve the
             Python object across multiple invocations.
"""

from typing import Iterator, IO, Optional
from ctypes import c_void_p, c_int, c_char_p, CFUNCTYPE
from pathlib import Path
from functools import reduce
from ._cobject import unref, ref_unref, CObjectWrapper
import dataclasses
import enum
import logging
import os
import time

logger = logging.getLogger("eis")

from snegg.c.libeis import libeis


class _LogPriority(enum.IntEnum):
    DEBUG = 10
    INFO = 20
    WARNING = 30
    ERROR = 40


class Error(Exception):
    def __init__(self, message: str, errno: Optional[int] = None):
        self.errno = errno
        self.message = message


class EventType(enum.IntEnum):
    CLIENT_CONNECT = 1
    CLIENT_DISCONNECT = 2
    SEAT_BIND = 3
    DEVICE_CLOSED = 4
    FRAME = 100
    DEVICE_START_EMULATING = 200
    DEVICE_STOP_EMULATING = 201

    POINTER_MOTION = 300
    POINTER_MOTION_ABSOLUTE = 400
    BUTTON_BUTTON = 500
    SCROLL_DELTA = 600
    SCROLL_STOP = 601
    SCROLL_CANCEL = 602
    SCROLL_DISCRETE = 603
    KEYBOARD_KEY = 700
    TOUCH_DOWN = 800
    TOUCH_UP = 801
    TOUCH_MOTION = 802


class DeviceCapability(enum.IntFlag):
    POINTER = 1 << 0
    POINTER_ABSOLUTE = 1 << 1
    KEYBOARD = 1 << 2
    TOUCH = 1 << 3
    SCROLL = 1 << 4
    BUTTON = 1 << 5

    @staticmethod
    def all() -> int:
        return reduce(lambda mask, x: x | mask, DeviceCapability)


class DeviceType(enum.IntEnum):
    VIRTUAL = 1
    PHYSICAL = 2


class KeymapType(enum.IntEnum):
    XKB = 1


@dataclasses.dataclass
class SeatEvent:
    capabilities: list[DeviceCapability]


@dataclasses.dataclass
class XkbModifiersEvent:
    depressed: int
    latched: int
    locked: int
    group: int


@dataclasses.dataclass
class KeyEvent:
    key: int
    is_press: bool


@dataclasses.dataclass
class ButtonEvent:
    button: int
    is_press: bool


@dataclasses.dataclass
class PointerEvent:
    dx: float
    dy: float


@dataclasses.dataclass
class PointerAbsoluteEvent:
    x: float
    y: float


@dataclasses.dataclass
class ScrollEvent:
    dx: float
    dy: float


@dataclasses.dataclass
class ScrollDiscreteEvent:
    dx: int
    dy: int


@dataclasses.dataclass
class ScrollStopEvent:
    x: bool
    y: bool


@dataclasses.dataclass
class TouchEvent:
    touchid: int
    x: float
    y: float


@unref(libeis)
class Event(CObjectWrapper):
    def __str__(self) -> str:
        return f"Event: {self.event_type.name}"

    def __repr__(self) -> str:
        return str(self)

    @property
    def client(self) -> "Client":
        c = libeis.event_get_client(self._cobject)
        return Client.instance(c)

    @property
    def event_type(self) -> EventType:
        return EventType(libeis.event_get_type(self._cobject))

    @property
    def time(self) -> int:
        return libeis.event_get_time(self._cobject)

    @property
    def device(self) -> Optional["Device"]:
        d = libeis.event_get_device(self._cobject)
        if d:
            device = Device.instance(d)
            return device
        else:
            return None

    @property
    def seat(self) -> Optional["Seat"]:
        s = libeis.event_get_seat(self._cobject)
        if s:
            seat = Seat.instance(s)
            return seat
        else:
            return None

    @property
    def seat_event(self) -> SeatEvent:
        return SeatEvent(
            capabilities=[
                c
                for c in DeviceCapability
                if libeis.event_seat_has_capability(self._cobject, c)
            ]
        )

    @property
    def emulating_sequence(self) -> int:
        return libeis.event_emulating_get_sequence(self._cobject)

    @property
    def key_event(self) -> KeyEvent:
        return KeyEvent(
            key=libeis.event_keyboard_get_key(self._cobject),
            is_press=libeis.event_keyboard_get_key_is_press(self._cobject),
        )

    @property
    def button_event(self) -> ButtonEvent:
        return ButtonEvent(
            button=libeis.event_button_get_button(self._cobject),
            is_press=libeis.event_button_get_is_press(self._cobject),
        )

    @property
    def pointer_event(self) -> PointerEvent:
        return PointerEvent(
            dx=libeis.event_pointer_get_dx(self._cobject),
            dy=libeis.event_pointer_get_dy(self._cobject),
        )

    @property
    def pointer_absolute_event(self) -> PointerAbsoluteEvent:
        return PointerAbsoluteEvent(
            x=libeis.event_pointer_get_absolute_x(self._cobject),
            y=libeis.event_pointer_get_absolute_y(self._cobject),
        )

    @property
    def scroll_event(self) -> ScrollEvent:
        return ScrollEvent(
            dx=libeis.event_scroll_get_dx(self._cobject),
            dy=libeis.event_scroll_get_dy(self._cobject),
        )

    @property
    def scroll_discrete_event(self) -> ScrollDiscreteEvent:
        return ScrollDiscreteEvent(
            dx=libeis.event_scroll_get_discrete_dx(self._cobject),
            dy=libeis.event_scroll_get_discrete_dy(self._cobject),
        )

    @property
    def scroll_stop_event(self) -> ScrollStopEvent:
        return ScrollStopEvent(
            x=libeis.event_scroll_get_stop_x(self._cobject),
            y=libeis.event_scroll_get_stop_y(self._cobject),
        )

    @property
    def touch_event(self) -> TouchEvent:
        return TouchEvent(
            touchid=libeis.event_touch_get_id(self._cobject),
            x=libeis.event_touch_get_x(self._cobject),
            y=libeis.event_touch_get_y(self._cobject),
        )


@dataclasses.dataclass(eq=True)
class Position:
    x: int
    y: int


@dataclasses.dataclass(eq=True)
class Dimension:
    width: int
    height: int


@dataclasses.dataclass(eq=True)
class ConfigureRegion:
    dimension: Dimension
    position: Position = dataclasses.field(default_factory=lambda: Position(0, 0))
    physical_scale: float = 1.0
    mapping_id: Optional[str] = None

    def __str__(self):
        return f"{self.dimension.width}x{self.dimension.height}@{self.position.x},{self.position.y}:s{self.physical_scale}|mid<{self.mapping_id}>"


@dataclasses.dataclass
class ConfigureKeymap:
    keymap_type: KeymapType
    fd: IO
    size: int


@ref_unref(libeis)
class Seat(CObjectWrapper):
    def __str__(self) -> str:
        return f"Seat: {self.name} {'|'.join(c.name for c in self.capabilities)}"

    @property
    def name(self) -> str:
        return libeis.seat_get_name(self._cobject).decode("utf-8")

    @property
    def client(self) -> "Client":
        return Client.instance(libeis.seat_get_name(self._cobject))

    @property
    def capabilities(self) -> tuple[DeviceCapability, ...]:
        return tuple(
            c for c in DeviceCapability if libeis.seat_has_capability(self._cobject, c)
        )

    def add(self):
        libeis.seat_add(self._cobject)

    def remove(self):
        libeis.seat_remove(self._cobject)

    def new_device(
        self,
        *,
        name: Optional[str] = None,
        device_type: DeviceType = DeviceType.VIRTUAL,
        size: Optional[Dimension] = None,
        capabilities: Optional[list[DeviceCapability]] = None,
        regions: list[ConfigureRegion] = [],
        keymap: Optional[ConfigureKeymap] = None,
    ) -> "Device":
        device = libeis.seat_new_device(self._cobject)
        assert device is not None

        if name:
            libeis.device_configure_name(device, name.encode("utf-8"))
        if device_type:
            libeis.device_configure_type(device, device_type)
        if size:
            libeis.device_configure_size(device, size.width, size.height)
        if capabilities is None:
            capabilities = list(self.capabilities)
        for c in capabilities:
            libeis.device_configure_capability(device, c)

        for r in regions:
            cregion = libeis.device_new_region(device)
            assert cregion is not None
            libeis.region_set_size(cregion, r.dimension.width, r.dimension.height)
            libeis.region_set_offset(cregion, r.position.x, r.position.y)
            libeis.region_set_physical_scale(cregion, r.physical_scale)
            try:
                if r.mapping_id:
                    libeis.region_set_mapping_id(cregion, r.mapping_id.encode("utf-8"))
            except NotImplementedError:
                # libei 1.0
                pass
            libeis.region_add(cregion)
        if keymap:
            ckm = libeis.device_new_keymap(
                device, keymap.keymap_type, keymap.fd.fileno(), keymap.size
            )
            assert ckm is not None
            libeis.keymap_add(ckm)
        return Device.instance(device)


@ref_unref(libeis)
class Device(CObjectWrapper):
    def __init__(self, cobject):
        super().__init__(cobject)
        self._seat = Seat.instance(libeis.device_get_seat(self._cobject))
        self._name = libeis.device_get_name(self._cobject).decode("utf-8")
        self._device_type = DeviceType(libeis.device_get_type(self._cobject))
        self._width = libeis.device_get_width(self._cobject)
        self._height = libeis.device_get_height(self._cobject)
        self._keymap = libeis.device_keyboard_get_keymap(self._cobject)
        self._capabilities = tuple(
            c
            for c in DeviceCapability
            if libeis.device_has_capability(self._cobject, c)
        )

        def regions():
            idx = 0
            while True:
                region = libeis.device_get_region(self._cobject, idx)
                if not region:
                    break
                yield region
                idx += 1

        self._regions = tuple(Region.instance(r) for r in regions())

    def __str__(self) -> str:
        return f"Device: {self.name} <{self.device_type.name}> {'|'.join(c.name for c in self.capabilities)} {'||'.join([str(r) for r in self.regions])}"

    @property
    def device_type(self) -> DeviceType:
        return self._device_type

    @property
    def name(self) -> str:
        return self._name

    @property
    def width(self) -> int:
        return self._width

    @property
    def height(self) -> int:
        return self._height

    @property
    def capabilities(self) -> tuple[DeviceCapability]:
        return self._capabilities

    @property
    def regions(self) -> tuple["Region"]:
        return self._regions

    @property
    def keyboard_get_keymap(self) -> Optional["Keymap"]:
        return self._keymap

    @property
    def seat(self) -> Seat:
        return self._seat

    def add(self):
        libeis.device_add(self._cobject)

    def remove(self):
        libeis.device_remove(self._cobject)

    def pause(self):
        libeis.device_pause(self._cobject)

    def resume(self):
        libeis.device_resume(self._cobject)

    def keyboard_xkb_modifiers(self, mods: XkbModifiersEvent):
        libeis.device_keyboard_send_xkb_modifiers(
            self._cobject, mods.depressed, mods.latched, mods.locked, mods.group
        )

    def start_emulating(self, sequence: Optional[int] = None) -> "Device":
        if sequence is None:
            sequence = int(time.time())
        libeis.device_start_emulating(self._cobject, sequence)
        return self

    def stop_emulating(self) -> "Device":
        libeis.device_stop_emulating(self._cobject)
        return self

    def frame(self, timestamp: Optional[int] = None) -> "Device":
        if timestamp is None:
            timestamp = libeis.now(libeis.device_get_context(self._cobject))
        libeis.device_frame(self._cobject, timestamp)
        return self

    def pointer_motion(self, x: float, y: float) -> "Device":
        libeis.device_pointer_motion(self._cobject, x, y)
        return self

    def pointer_motion_absolute(self, x: float, y: float) -> "Device":
        libeis.device_pointer_motion_absolute(self._cobject, x, y)
        return self

    def button_button(self, button: int, is_press: bool) -> "Device":
        libeis.device_button_button(self._cobject, button, is_press)
        return self

    def keyboard_key(self, key: int, is_press: bool) -> "Device":
        libeis.device_keyboard_key(self._cobject, key, is_press)
        return self

    def touch_new(self) -> "Touch":
        return Touch.instance(libeis.device_touch_new(self._cobject))


@ref_unref(libeis)
class Region(CObjectWrapper):
    def __init__(self, cobject):
        super().__init__(cobject)
        x = libeis.region_get_x(self._cobject)
        y = libeis.region_get_y(self._cobject)
        self._position = Position(x, y)
        w = libeis.region_get_width(self._cobject)
        h = libeis.region_get_height(self._cobject)
        self._dimension = Dimension(w, h)
        self._scale = libeis.region_get_physical_scale(self._cobject)
        try:
            mid = libeis.region_get_mapping_id(self._cobject)
            self._mapping_id = mid.decode("utf-8") if mid else None
        except NotImplementedError:
            self._mapping_id = None

    def __str__(self):
        return f"{self.dimension.width}x{self.dimension.height}@{self.position.x},{self.position.y}:s{self.physical_scale}|mid<{self.mapping_id}>"

    @property
    def position(self) -> Position:
        return self._position

    @property
    def dimension(self) -> Dimension:
        return self._dimension

    @property
    def physical_scale(self) -> float:
        return self._scale

    @property
    def mapping_id(self) -> Optional[str]:
        return self._mapping_id

    def contains(self, x: float, y: float) -> bool:
        return libeis.region_contains(self._cobject, x, y)


@ref_unref(libeis)
class Keymap(CObjectWrapper):
    @property
    def keymap_type(self) -> KeymapType:
        return KeymapType(libeis.keymap_get_type(self._cobject))

    @property
    def device(self) -> Device:
        return Device.instance(libeis.keymap_get_device(self._cobject))

    @property
    def size(self) -> int:
        return libeis.keymap_get_size(self._cobject)

    @property
    def fd(self) -> IO:
        return os.fdopen(libeis.keymap_get_fd(self._cobject))


@unref(libeis)
class Touch(CObjectWrapper):
    @property
    def device(self) -> Device:
        return Device.instance(libeis.touch_get_device(self._cobject))

    def down(self, x: float, y: float) -> "Device":
        libeis.touch_down(self._cobject, x, y)
        return self.device

    def motion(self, x: float, y: float) -> "Device":
        libeis.touch_motion(self._cobject, x, y)
        return self.device

    def up(self) -> "Device":
        libeis.touch_up(self._cobject)
        return self.device


@ref_unref(libeis)
class Client(CObjectWrapper):
    @property
    def is_sender(self) -> bool:
        return libeis.client_is_sender(self._cobject)

    @property
    def name(self) -> str:
        return libeis.client_get_name(self._cobject).decode("utf-8")

    def connect(self):
        libeis.client_connect(self._cobject)

    def disconnect(self):
        libeis.client_disconnect(self._cobject)

    def new_seat(
        self,
        *,
        name: str = "default seat",
        capabilities: tuple[DeviceCapability, ...] = tuple(DeviceCapability),
    ):
        seat = libeis.client_new_seat(self._cobject, name.encode("utf-8"))
        assert seat is not None
        for c in capabilities:
            libeis.seat_configure_capability(seat, c)
        return Seat.instance(seat)


@CFUNCTYPE(None, c_void_p, c_int, c_char_p, c_void_p)
def loghandler(eis_context, priority, message, log_context):
    level = {
        _LogPriority.DEBUG: logging.DEBUG,
        _LogPriority.INFO: logging.INFO,
        _LogPriority.WARNING: logging.WARNING,
        _LogPriority.ERROR: logging.ERROR,
    }.get(priority, logging.DEBUG)
    logger.log(level, message.decode("utf-8"))


@unref(libeis, unref_func="unref")
class Eis(CObjectWrapper):
    def __init__(self):
        cobject = libeis.new(None)
        libeis.log_set_handler(cobject, loghandler)
        libeis.log_set_priority(cobject, _LogPriority.DEBUG)
        self._fd = libeis.get_fd(cobject)
        super().__init__(cobject)

    @property
    def fd(self) -> int:
        assert self._fd is not None
        return self._fd

    @property
    def events(self) -> Iterator[Event]:
        while True:
            e = libeis.get_event(self._cobject)
            if not e:
                break
            yield Event(e)

    @property
    def now(self) -> int:
        return libeis.now(self._cobject)

    def dispatch(self) -> None:
        libeis.dispatch(self._cobject)

    def add_client_fd(self, fd: IO):
        err = libeis.backend_fd_add_client(self, fd.fileno())
        if err < 0:
            raise Error(os.strerror(-err), -err)

    @classmethod
    def create_for_fd(cls, path: Optional[str]) -> "Eis":
        ctx = cls()
        err = libeis.setup_backend_fd(ctx._cobject)
        if err < 0:
            raise Error(os.strerror(-err), -err)
        return ctx

    @classmethod
    def create_for_socket(cls, path: Path) -> "Eis":
        ctx = cls()
        err = libeis.setup_backend_socket(ctx._cobject, os.fspath(path).encode("utf-8"))
        if err < 0:
            raise Error(os.strerror(-err), -err)
        return ctx
