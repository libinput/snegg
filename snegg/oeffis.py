# Copyright © 2023 Red Hat, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# This file is formatted with Python black

"""
Wrapper module around the ``liboeffis`` C library. This is a thin API wrapper with
most of the semantics of the underlying C library preserved.
See the `liboeffis documentation <https://libinput.pages.freedesktop.org/libei/api/group__liboeffis.html>`_
for details on each API.

.. warning:: Most objects in this module are refcounted and automatically destroy
             the underlying C object when the Python reference is dropped.
             This may cause all sub-objects in the underlying C object to be
             destroyed as well. Care must be taken to preserve the
             Python object across multiple invocations.
"""

from typing import IO, Optional
from snegg.c.liboeffis import liboeffis

import enum
import os


class DisconnectedError(Exception):
    """
    Raised when our context is disconnected from the bus. This
    error is raised when the connection encountered some error,
    e.g. a misconfiguration or access errors.

    See :class:`SessionClosedError` for the exception raised
    when our session was explicitly closed.

    .. attribute:: message

                A message to indicate the cause of the disconnection.
    """

    def __init__(self, message):
        super().__init__()
        self.message = message


class SessionClosedError(DisconnectedError):
    """
    Raised when our session was closed by the XDG Desktop Portal.
    This may happen in response to some user interaction but is
    not typically an indication of an error. See :class:`DisconnectedError`
    for the exception raised when an error occurs.
    """

    def __init__(self):
        super().__init__(message="Session closed")


class DeviceType(enum.IntFlag):
    """
    The set of devices to request from the RemoteDesktop session.
    """

    ALL_DEVICES = 0
    KEYBOARD = 1
    POINTER = 2
    TOUCHSCREEN = 4


class _EventType(enum.IntEnum):
    NONE = 0
    CONNECTED_TO_EIS = 1
    CLOSED = 2
    DISCONNECTED = 3


class Oeffis:
    """
    Wrapper class around a liboeffis context. See the liboeffis documentation for
    `oeffis_new()` for details:
    https://libinput.pages.freedesktop.org/libei/api/group_liboeffis.html

    This is a Pythonic wrapper, after creating a session use the `fd` property
    to monitor for events and call `dispatch()` in response to any events on that
    file descriptor.

    Note that the :class:`Oeffis` context must be kept alive for the duration
    of the session, destroying the context results in disconnection and
    thus the EIS fd becoming invalid for any libei contexts that use that fd.
    """

    def __init__(self):
        self._ctx = liboeffis.new(None)
        self._fd = os.fdopen(liboeffis.get_fd(self._ctx))
        self._eis_fd: Optional[IO] = None
        self._state = _EventType.NONE

    def __del__(self):
        liboeffis.unref(self._ctx)

    @property
    def fd(self) -> IO:
        """
        The file descriptor to monitor for incoming events. Whenever an event
        occurs on this file descriptor, call :meth:`dispatch` immediately to
        process the events.
        """
        return self._fd

    @property
    def eis_fd(self) -> IO:
        """
        The file descriptor to pass to :class:`snegg.ei.Sender.create_for_fd`.
        This file descriptor does not exist until :meth:`dispatch` returns
        ``True``.

        .. warning:: Reading this property descriptor raises a
                    :class:`DisconnectedError` if invoked before
                    :meth:`dispatch` returns ``True``
        """
        if self._state != _EventType.CONNECTED_TO_EIS:
            raise DisconnectedError(self.error_message)
        assert self._eis_fd is not None
        return self._eis_fd

    def dispatch(self) -> bool:
        """
        Dispatch any pending events and return ``True`` if we are connected, or
        ``False`` if there's nothing happening right now.

        If we get disconnected and/or the session is closed, this function raises
        a :class:`DisconnectedError` or :class:`SessionClosedError`, respectively.
        Calling dispatch after getting disconnected/closed will result in the same
        exceptions being thrown.
        """
        if self._state == _EventType.CLOSED:
            raise SessionClosedError()
        elif self._state == _EventType.DISCONNECTED:
            raise DisconnectedError(message=self.error_message)

        liboeffis.dispatch(self._ctx)
        e = liboeffis.get_event(self._ctx)
        while e:
            event = _EventType(e)
            if event == _EventType.CONNECTED_TO_EIS:
                eis_fd = liboeffis.get_eis_fd(self._ctx)
                if eis_fd is None:
                    raise DisconnectedError(message="Failed to obtain the EIS fd")
                self._eis_fd = os.fdopen(liboeffis.get_eis_fd(self._ctx))
                self._state = _EventType.CONNECTED_TO_EIS
                return True
            elif event == _EventType.DISCONNECTED:
                self._state = _EventType.DISCONNECTED
                raise DisconnectedError(self.error_message)
            elif event == _EventType.CLOSED:
                self._state = _EventType.CLOSED
                raise SessionClosedError()

        return False

    @property
    def error_message(self) -> Optional[str]:
        """
        Returns the error message that caused this context to disconnect.
        In most cases this is the same error that was already raised
        in the :class:`DisconnectedError` exception.

        This method returns `None` if our context is still connected.
        """
        return liboeffis.get_error_message(self._ctx).decode("utf-8")

    @classmethod
    def create(
        cls,
        devices: int = DeviceType.ALL_DEVICES,
        busname: str = "org.freedesktop.portal.Desktop",
    ) -> "Oeffis":
        """
        Create a liboeffis context for the given set of devices on the given
        busname.
        """
        oeffis = cls()
        liboeffis.create_session_on_bus(oeffis._ctx, busname.encode("utf-8"), devices)
        return oeffis
